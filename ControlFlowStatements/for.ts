// Cú pháp của câu lệnh 'for' vòng lặp TypeScript

`for(initialization; condition; expression) {
    // statement
}`

// TypeScript cho phép bạn bỏ qua hoàn toàn phần nội dung vòng lặp:
`for(initialization; condition; expression);`
// Tuy nhiên, nó hiếm khi được sử dụng trong thực tế vì nó làm cho mã khó đọc và khó bảo trì hơn

/**
 *  Simple TypeScript for example
 */
 for (let i = 0; i < 10; i++) {
    console.log(i);
}

/**
 *  TypeScript for example: optional block
 */
 let i = 0;
 for (; ;) { // Giống như khối initialization, bạn có thể bỏ qua khối condition.
        console.log(i);
        i++;
        if (i > 9) break;
        // Bạn phải thoát khỏi vòng lặp khi một điều kiện được đáp ứng bằng cách sử dụng câu lệnh ifvà break
        // Nếu không, bạn sẽ tạo ra một vòng lặp vô hạn 
 }