/**
 *  Câu lệnh break cho phép bạn kết thúc một vòng lặp
 *  và chuyển quyền điều khiển chương trình cho câu lệnh tiếp theo sau vòng lặp.
 */

// Sử dụng câu lệnh break bên trong một vòng lặp for:
 let products1 = [
    { name: 'phone', price: 700 },
    { name: 'tablet', price: 900 },
    { name: 'laptop', price: 1200 }
];

for (var a = 0; a < products1.length; a++) {
    if (products1[a].price == 900)
        break;
}
console.log(products1[a]);

// Sử dụng câu lệnh break để thoát ra khỏi switch:
let products = [
    { name: 'phone', price: 700 },
    { name: 'tablet', price: 900 },
    { name: 'laptop', price: 1200 }
];

let discount = 0;
let product = products[1];

switch (product.name) {
    case 'phone':
        discount = 5;
        break;
    case 'tablet':
        discount = 10;
        break;
    case 'laptop':
        discount = 15;
        break;
}

console.log(`There is a ${discount}% on ${product.name}.`);