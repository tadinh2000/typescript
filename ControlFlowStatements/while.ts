// Câu lệnh while cho phép bạn tạo một vòng lặp thực thi một khối mã 
// miễn là có điều kiện true.
`while(condition) {
    // do something
}`

// Để phá vỡ vòng lặp chưa chín muồi dựa trên một điều kiện khác,
// bạn sử dụng câu lệnh 'if' and 'break':
`while(condition) {
    // do something
    // ...

    if(anotherCondition) 
        break;
}`

let counter = 0;

while (counter < 5) {
    console.log(counter);
    counter++;
}
