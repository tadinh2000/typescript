// Một câu lệnh if thực hiện một câu lệnh dựa trên một điều kiện.
// Nếu điều kiện là true, câu lệnh if sẽ thực thi các câu lệnh bên trong phần thân của nó
const max = 100;
let counter = 0;

if (counter < max) {
    counter++;
}

console.log(counter); // 1

// Nếu bạn muốn thực thi các câu lệnh khác khi điều kiện trong câu lệnh if đánh giá là false,
// bạn có thể sử dụng câu lệnh if...else:
const max2 = 100;
let counter2 = 100;

if (counter2 < max2) {
    counter2++;
} else {
    counter2 = 1;
}

console.log(counter2);

//  TOÁN TỬ BẬC 3
// Trong thực tế, nếu bạn có một điều kiện đơn giản,
// bạn có thể sử dụng toán tử bậc ba ?: thay vì câu lệnh if...else để làm cho mã ngắn hơn
const max3 = 100;
let counter3 = 100;

counter3 < max3 ? counter3++ : counter3 = 1;

console.log(counter3);

// CÂU LỆNH TYPESCRIPT IF...ELSE IF...ELSE
// Khi bạn muốn thực thi mã dựa trên nhiều điều kiện, bạn có thể sử dụng câu lệnh if...else if...else
let discount: number;
let itemCount = 11;

if (itemCount > 0 && itemCount <= 5) {
    discount = 5;  // 5% discount
} else if (itemCount > 5 && itemCount <= 10) {
    discount = 10; // 10% discount 
} else {
    discount = 15; // 15%
}

console.log(`You got ${discount}% discount. `)