let message: string = 'Hello, World!';
// create a new heading 1 element
let heading = document.createElement('h1');
heading.textContent = message;
// add the heading the document
document.body.appendChild(heading);

// Trong JavaScript
function getProductJavaScript(id){
    return {
        id: id,
        name: `Awesome Gadget ${id}`,
        price: 99.5
    }
};
const productJavaScript = getProductJavaScript(1);
console.log(`The product ${productJavaScript.name} const ${productJavaScript.price}`);

// Trong TypeScript
interface ProductTypeScrip{
    id: number,
    name: string,
    price: number
};
function getProductTypeScrip(id) : ProductTypeScrip{
    return {
        id: id,
        name: `Awesome Gadget ${id}`,
        price: 99.5
    }
}
const productTypeScrip = getProductTypeScrip(2);
console.log(`The product ${productTypeScrip.name} const ${productTypeScrip.price}`);