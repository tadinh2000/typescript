var message = 'Hello, World!';
// create a new heading 1 element
var heading = document.createElement('h1');
heading.textContent = message;
// add the heading the document
document.body.appendChild(heading);
// Trong JavaScript
function getProductJavaScript(id) {
    return {
        id: id,
        name: "Awesome Gadget ".concat(id),
        price: 99.5
    };
}
;
var productJavaScript = getProductJavaScript(1);
console.log("The product ".concat(productJavaScript.name, " const ").concat(productJavaScript.price));
;
function getProductTypeScrip(id) {
    return {
        id: id,
        name: "Awesome Gadget ".concat(id),
        price: 99.5
    };
}
var productTypeScrip = getProductTypeScrip(2);
console.log("The product ".concat(productTypeScrip.name, " const ").concat(productTypeScrip.price));
