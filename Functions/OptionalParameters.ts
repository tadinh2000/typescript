// THAM SỐ TÙY CHỌN

/**
 *  Để làm cho một tham số hàm là tùy chọn, bạn sử dụng ? sau tên tham số
 */
function multiply(a: number, b: number, c?: number): number {

    if (typeof c !== 'undefined') {
        return a * b * c;
    }
    return a * b;
}
/**
 *  Đầu tiên, sử dụng tham số ? sau c
 *  Thứ hai, kiểm tra xem đối số có được truyền vào hàm hay không bằng cách sử dụng biểu thức typeof c !== 'undefined'
 */