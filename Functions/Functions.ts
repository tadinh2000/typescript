// Giống như JavaScript, bạn sử dụng từ khóa function để khai báo một hàm trong TypeScript
`function name(parameter: type, parameter:type,...): returnType {
    // do something
}`


//  TypeScript cho phép bạn sử dụng chú thích kiểu trong các tham số và giá trị trả về của một hàm.
function add(a: number, b: number): number {
    return a + b;
}
// Sau dấu ngoặc đơn : number cho biết kiểu trả về. 
// Hàm add() trả về một giá trị kiểu number trong trường hợp này.

// Từ khóa void cho biết rằng hàm không trả về bất kỳ giá trị nào
function echo(message: string): void {
    console.log(message.toUpperCase());
}

/**
 *  - Khi bạn không chú thích kiểu trả về, TypeScript sẽ cố gắng suy ra một kiểu thích hợp.
 *  - Nếu một hàm có các nhánh khác nhau trả về các kiểu khác nhau, 
 *      trình biên dịch TypeScript có thể suy ra kiểu union hoặc kiểu any.
 */