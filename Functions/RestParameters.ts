/**
 *  - Rest Parameter (hay còn gọi là đại diện của các tham số còn lại) 
 *      cho phép bạn biểu diễn một số lượng vô hạn đối số dưới dạng một mảng. 
 *  - Tham số cuối cùng (args) được bắt đầu bằng dấu ba chấm ( ... ) 
 *      được gọi là Rest Parameter ( ... args ).
 *  - Tham số phần còn lại cho phép bạn một hàm chấp nhận không 
 *      hoặc nhiều đối số của kiểu được chỉ định.
 */


/**
 *  Các tham số còn lại tuân theo các quy tắc sau:
 *  - Một hàm chỉ có một tham số còn lại.
 *  - Tham số còn lại xuất hiện cuối cùng trong danh sách tham số.
 *  - Kiểu của tham số còn lại là kiểu mảng .
 */

 function getTotal(...numbers: number[]): number {
    let total = 0;
    numbers.forEach((num) => total += num);
    return total;
}
console.log(getTotal()); // 0
console.log(getTotal(10, 20)); // 30
console.log(getTotal(10, 20, 30)); // 60