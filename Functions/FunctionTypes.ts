/**
 *  Một kiểu hàm có hai phần: tham số và kiểu trả về.
 *  Khi khai báo một kiểu hàm, bạn cần chỉ định cả hai phần theo cú pháp sau:
 *  (parameter: type, parameter:type,...) => type
 */

// CÁCH 1
let add_vd2: (x: number, y: number) => number;
/**
 *  - Kiểu hàm chấp nhận hai đối số: x và y với kiểu number.
 *  - Kiểu của giá trị trả về là number theo sau mũi tên (=>) xuất hiện giữa các tham số và kiểu trả về.
 */
add_vd2 = function (x: number, y: number) {
    return x + y;
};

// CÁCH 2
let add_vd3: (a: number, b: number) => number =
    function (x: number, y: number) {
        return x + y;
    };