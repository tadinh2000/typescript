// CÁC KIỂU GIAO NHAU TRONG TYPESCRIPT

// Để kết hợp các loại, bạn sử dụng &toán tử như sau:
'type typeAB = typeA & typeB;'

// Kiểu liên hợp sử dụng |toán tử xác định một biến có thể giữ giá trị của một trong hai
'let varName = typeA | typeB;'

// Ví dụ:
interface BusinessPartner1 {
    name: string;
    credit: number;
}

interface Identity {
    id: number;
    name: string;
}

interface Contact {
    email: string;
    phone: string;
}
// Kiểu Employee chứa tất cả các thuộc tính của kiểu Identity và Contact:
type Employee = Identity & Contact;

let e: Employee = {
    id: 100,
    name: 'John Doe',
    email: 'john.doe@example.com',
    phone: '(408)-897-5684'
};
// Kiểu Customer chứa tất cả các thuộc tính của kiểu BusinessPartner và Contact:
type Customer1 = BusinessPartner1 & Contact;

let c: Customer1 = {
    name: 'ABC Inc.',
    credit: 1000000,
    email: 'sales@abcinc.com',
    phone: '(408)-897-5735'
};

