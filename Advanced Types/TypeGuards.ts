// Type Guards cho phép bạn thu hẹp loại biến trong một khối có điều kiện .

/**
 *     typeof
 */

// Xác định kiểu alphanumeric có thể chứa một chuỗi hoặc một số .
type alphanumeric = string | number;
// Khai báo một hàm có thêm hai biến a và b với kiểu alphanumeric
function add(a: alphanumeric, b: alphanumeric) {
    // Kiểm tra xem cả hai loại đối số có phải là số hay không
    if (typeof a === 'number' && typeof b === 'number') {
        // Nếu có, hãy tính tổng các đối số bằng toán tử +.
        return a + b;
    }
    // Kiểm tra xem cả hai loại đối số có phải là chuỗi hay không
    if (typeof a === 'string' && typeof b === 'string') {
        // Nếu có, sau đó nối hai đối số.
        return a.concat(b);
    }
    // Đưa ra một lỗi nếu các đối số không phải là số hoặc chuỗi.
    throw new Error('Invalid arguments. Both arguments must be either numbers or strings.');
}


/**
 *     instanceof
 */

// Khai báo các lớp Customervà Supplier.
class Customer {
    isCreditAllowed(): boolean {
        // ...
        return true;
    }
}

class Supplier {
    isInShortList(): boolean {
        // ...
        return true;
    }
}
// Tạo một bí danh kiểu BusinessPartner là kiểu kết hợp của Customer và Supplier.
type BusinessPartner = Customer | Supplier;
// Khai báo một hàm signContract() chấp nhận một tham số với kiểu BusinessPartner.
function signContract(partner: BusinessPartner) : string {
    let message: string;
    // Kiểm tra xem đối tác có phải là một trường hợp của Customer hoặc không Supplier
    if (partner instanceof Customer) {
        message = partner.isCreditAllowed() ? 'Sign a new contract with the customer' : 'Credit issue';
    }

    if (partner instanceof Supplier) {
        message = partner.isInShortList() ? 'Sign a new contract the supplier' : 'Need to evaluate further';
    }

    return message;
}


/**
 *     in
 */
// in điều hành thực hiện kiểm tra an toàn cho sự tồn tại của một thuộc tính trên một đối tượng.
function signContract_in(partner: BusinessPartner) : string {
    let message: string;
    if ('isCreditAllowed' in partner) {
        message = partner.isCreditAllowed() ? 'Sign a new contract with the customer' : 'Credit issue';
    } else {
        // must be Supplier
        message = partner.isInShortList() ? 'Sign a new contract the supplier ' : 'Need to evaluate further';
    }
    return message;
}
