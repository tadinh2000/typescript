// Khẳng định kiểu còn được gọi là thu hẹp kiểu.
// Nó cho phép bạn thu hẹp một loại từ một loại liên hợp .
function getNetPrice(price: number, discount: number, format: boolean): number | string {
    let netPrice = price * (1 - discount);
    return format ? `$${netPrice}` : netPrice;
}
// Sử dụng từ khóa as để hướng dẫn trình biên dịch rằng giá trị được gán cho netPrice là một chuỗi
let netPrice = getNetPrice(100, 0.05, true) as string;
console.log(netPrice);

// Sử dụng từ khóa as để hướng dẫn trình biên dịch rằng giá trị trả về của hàm getNetPrice() là một số.
let netPrice2 = getNetPrice(100, 0.05, false) as number;
console.log(netPrice2);


// Cú pháp xác nhận loại thay thế
// Bạn cũng có thể sử dụng cú pháp dấu ngoặc nhọn <>để xác nhận một kiểu, như sau:
'<targetType> value'
let netPrice3 = <number>getNetPrice(100, 0.05, false);
