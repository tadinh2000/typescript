// Type Casting cho phép bạn chuyển đổi một biến từ kiểu này sang kiểu khác.
// Bạn có thể sử dụng từ khóa as hoặc toán tử <>


//Nhập truyền bằng từ khóa as
/**
 *  Sử dụng kiểu ép kiểu truyền Phần tử tới HTMLInputElement 
 *   bằng cách sử dụng astừ khóa như sau:
 */
let input = document.querySelector('input[type="text"]') as HTMLInputElement;
console.log(input.value);
// Một cách khác để truyền Elementđến HTMLInputElementlà khi bạn truy cập thuộc tính như sau:
let enteredText = (input as HTMLInputElement).value;
// Cú pháp để chuyển đổi một biến từ typeAthành typeBnhư sau:
`let a: typeA;
let b = a as typeB;
`


// Nhập Truyền bằng toán tử <>
let input2 = <HTMLInputElement>document.querySelector('input[type="text"]');
console.log(input2.value);
// Cú pháp để truyền kiểu sử dụng <>là:
`let a: typeA;
 let b = <typeB>a;`