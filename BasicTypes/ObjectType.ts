// Kiểu TypeScript objectđại diện cho tất cả các giá trị không có trong kiểu nguyên thủy.
// Sau đây là cách khai báo một biến chứa một đối tượng:
let employee: object;

employee = {
    firstName: 'Dinh',
    lastName: 'Ta Van',
    age: 23,
    jobTitle: 'Web Developer'
}
console.log(employee);

// Để chỉ định rõ ràng các thuộc tính của employeeđối tượng,
// trước tiên bạn sử dụng cú pháp sau để khai báo employeeđối tượng:
let employee2: {
    first_name: string,
    last_name: string,
    age2: number,
    job_title: string
} = {
    first_name: 'Dinh',
    last_name: 'Ta Van',
    age2: 23,
    job_title: 'Web Developer'
}
console.log(employee2);

// Loại trống {}
// Kiểu trống {}mô tả một đối tượng không có thuộc tính của riêng nó. 
// Nhưng bạn có thể truy cập tất cả các thuộc tính và phương thức được khai báo trên Objectkiểu,
// có sẵn trên đối tượng thông qua chuỗi nguyên mẫu :
let vacant: {} = {};

console.log(vacant.toString());
// Dau ra: [object Object]