/**
 *  Loại kết hợp
 */

// Biến sau thuộc loại numberhoặc string:
let resultUnion: number | string;
resultUnion = 10; // OK
resultUnion = 'Hi'; // also OK
resultUnion = false; // a boolean value, not OK

// Bạn có thể thay đổi kiểu của các tham số từ anyunion thành union như sau:
function add(a: number | string, b: number | string) {
    if (typeof a === 'number' && typeof b === 'number') {
        return a + b;
    }
    if (typeof a === 'string' && typeof b === 'string') {
        return a.concat(b);
    }
    throw new Error('Parameters must be numbers or strings');
}