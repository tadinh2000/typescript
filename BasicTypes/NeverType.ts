/**
 *  - Loại neverlà một loại không chứa giá trị.
 *      Do đó, bạn không thể gán bất kỳ giá trị nào cho một biến có neverkiểu.
 */

// Thông thường, bạn sử dụng never kiểu để đại diện cho kiểu trả về của một hàm luôn tạo ra lỗi. 
// VD:
function raiseError(message: string): never {
    throw new Error(message);
}

// Kiểu trả về của hàm sau được suy ra là kiểu never:
function reject() { 
    return raiseError('Rejected');
}

// Nếu bạn có một biểu thức hàm chứa một vòng lặp không xác định, thì kiểu trả về của nó cũng là kiểu never
let loop = function forever() {
    while (true) {
        console.log('Hello');
    }
}

// Bạn có thể trả về một hàm có kiểu trả về là kiểu never.
function fn(a: string | number): boolean {
    if (typeof a === "string") {
      return true;
    } else if (typeof a === "number") {
      return false;
    }  
    // make the function valid
    return neverOccur();
  }
  
  let neverOccur = () => {
     throw new Error('Never!');
  } 
