// Một enum là một nhóm các giá trị hằng số được đặt tên. 
// Enum là viết tắt của kiểu liệt kê.

/**
 *  Để xác định một enum, bạn làm theo các bước sau:
 *  - Đầu tiên, sử dụng enumtừ khóa theo sau là tên của enum.
 *  - Sau đó, xác định các giá trị không đổi cho enum.
 */

// Cú pháp để xác định một enum:
'enum name {constant1, constant2, ...};'

// Ví dụ sau tạo một enum đại diện cho các tháng trong năm:
enum Month {
    Jan,
    Feb,
    Mar,
    Apr,
    May,
    Jun,
    Jul,
    Aug,
    Sep,
    Oct,
    Nov,
    Dec
};

// Khai báo một hàm sử dụng Monthenum làm kiểu monththam số:
function isItSummer(month: Month) {
    let isSummer: boolean;
    switch (month) {
        case Month.Jun:
        case Month.Jul:
        case Month.Aug:
            isSummer = true;
            break;
        default:
            isSummer = false;
            break;
    }
    return isSummer;
}
console.log(isItSummer(Month.Jun)); // true

/**
 *  Bạn nên sử dụng enum khi:
 *  - Có một tập hợp nhỏ các giá trị cố định có liên quan chặt chẽ với nhau
 *  - Và những giá trị này đã được biết tại thời điểm biên dịch.
 */
 enum ApprovalStatus {
    draft,
    submitted,
    approved,
    rejected
};
const request =  {
    id: 1,
    status: ApprovalStatus.approved,
    description: 'Please approve this request'
};

if(request.status === ApprovalStatus.approved) {
    // send an email
    console.log('Send email to the Applicant...');
}