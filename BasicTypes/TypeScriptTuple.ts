// Một bộ tuple hoạt động giống như một mảng với một số cân nhắc bổ sung:
// - Số lượng phần tử trong bộ được cố định.
// - Các loại phần tử đã biết và không cần phải giống nhau.
let skill: [string, number];
skill = ['Programming', 5];
// Thứ tự của các giá trị trong một bộ giá trị là quan trọng và cần phải giữ đúng thứ tự giá trị


// ---------------------------------------- //
// Phần tử Tuple tùy chọn
// một tuple có thể có các phần tử tùy chọn được chỉ định 
// bằng cách sử dụng hậu tố dấu hỏi 
'(?)'
let bgColor, headerColor: [number, number, number, number?];
bgColor = [0, 255, 255, 0.5];
headerColor = [0, 255, 255];