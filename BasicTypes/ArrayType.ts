// TypeScript arraylà một danh sách dữ liệu có thứ tự.
// Để khai báo một mảng chứa các giá trị của một kiểu cụ thể, bạn sử dụng cú pháp sau:
'let arrayName: type[];'

// Phần sau khai báo một mảng các chuỗi :
let skills: string[];
skills = ['Problem Sovling','Software Design','Programming'];
// Bạn có thể thêm một hoặc nhiều chuỗi vào mảng:
'skills[0] = "Problem Solving";'
'skills[1] = "Programming";'
// hoặc sử dụng phương pháp push():
"skills.push('Software Design');"
console.log(skills);


// --------------------------------------------- //
// Thuộc tính và phương thức mảng TypeScript
// Mảng TypeScript có thể truy cập các thuộc tính và phương thức của JavaScript.\
// Và bạn có thể sử dụng tất cả các phương thức mảng hữu ích như forEach(), map() và reduce(). filter()
let series = [1, 2, 3];
let doubleIt = series.map(e => e* 2);
console.log(doubleIt);


// --------------------------------------------- //
// Lưu trữ giá trị của các loại hỗn hợp
let scores = ['Programming', 5, 'Software Design', 4];
// tương tự:
let scores2 : (string | number)[];
scores2 = ['Programming', 5, 'Software Design', 4]; 