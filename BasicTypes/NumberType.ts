// Tất cả các số trong TypeScript đều là giá trị dấu phẩy động hoặc số nguyên lớn. 
// Các số dấu phẩy động có kiểu 'number' trong khi các số nguyên lớn có kiểu 'bigint'

// Kiểu số:
let price: 9.95;

// Số thập phân:
let x: number = 100, 
    y: number = 200;

// Số nhị phân: Số nhị phân sử dụng số 0 đứng đầu theo sau là chữ cái viết thường hoặc viết hoa “B”
let bin = 0b100;
let anotherBin: number = 0B010;

// Số bát phân: Một số bát phân sử dụng số 0 đứng đầu sau chữ cái 'o'
// Các chữ số sau '0o' là các số trong phạm vi 0 đến 7
let octal: number = 0o10;

// Số thập lục phân: sử dụng số 0 đứng đầu, theo sau là chữ thường hoặc chữ hoa 'x'
// Các chữ số sau dấu '0x' phải nằm trong khoảng ( 0123456789ABCDEF).
let hexadecimal: number = 0XA;

// Số nguyên lớn: Các số nguyên lớn đại diện cho các số nguyên lớn hơn 2 53  - 1.
// Một chữ số nguyên Lớn có nký tự ở cuối một chữ số nguyên như sau:
let big: bigint = 9007199254740991n;
