// Chú thích kiểu trong TypeScript
// - TypeScript sử dụng cú pháp': type'sau mã định danh làm chú thích kiểu, 
//      trong đó typecó thể là bất kỳ kiểu hợp lệ nào.
//
// Nhập chú thích vào các biến hằng số
//      - let variableName: type;
//      - let variableName: type = value;
//      - const constantName: type = value;
let first_name: string = 'John';
let age: number = 25;
let active: boolean = true;

// Mảng:        let arrayName: type[];
let names: string[] = ['John', 'Jane', 'Peter', 'David', 'Mary'];

// Các đối tượng:
let person: {
    name: string;
    age: number
};
 
person = {
    name: 'John',
    age: 25
};

// Đối số hàm & kiểu trả về:
let greeting : (name: string) => string;
greeting = function (name: string) {
    return `Hi ${name}`;
};

// Type Inference (Kiểu suy luận)
// Basic
let counter = 0;
// tương đương:
let counter: number = 0;

// Tương tự như vậy, khi bạn gán một giá trị cho tham số hàm, 
//      TypeScript suy ra kiểu của tham số thành kiểu của giá trị mặc định
function increment(counter: number) {
    return counter++;
}
// Tương tự:
function increment(counter: number) : number {
    return counter++;
}

// Thuật toán loại chung
// Nếu bạn thêm một chuỗi vào mảng ypeScript sẽ suy ra kiểu cho các mục dưới dạng một mảng số và chuỗi
let items = [0, 1, null, 'Hi'];
// Khi TypeScript không thể tìm thấy kiểu chung tốt nhất, nó sẽ trả về kiểu mảng liên hợp. Ví dụ:
let arr = [new Date(), new RegExp('\d+')];

// Nhập theo ngữ cảnh:
// TypeScript sử dụng vị trí của các biến để suy ra kiểu của chúng.
// Cơ chế này được gọi là nhập theo ngữ cảnh. Ví dụ:
document.addEventListener('click', function (event) {
    console.log(event.button); // 
});
