// Chú thích kiểu trong TypeScript
// - TypeScript sử dụng cú pháp': type'sau mã định danh làm chú thích kiểu, 
//      trong đó typecó thể là bất kỳ kiểu hợp lệ nào.
//
// Nhập chú thích vào các biến hằng số
//      - let variableName: type;
//      - let variableName: type = value;
//      - const constantName: type = value;
var first_name = 'John';
var age = 25;
var active = true;
