// Không tham gia kiểm tra kiểu
// và cho phép giá trị chuyển qua kiểm tra thời gian biên dịch.

// Kiểu 'any' cho phép bạn gán giá trị thuộc bất kỳ kiểu nào cho một biến:
// json may come from a third-party API
const json = `{"latitude": 10.11, "longitude":12.12}`;

// parse JSON to find location
const currentLocation = JSON.parse(json);
console.log(currentLocation);


// Nhập ẩn
/**
 *  Nếu bạn khai báo một biến mà không chỉ định kiểu, 
 *  TypeScript sẽ giả định rằng bạn sử dụng anykiểu. 
 *  Tính năng này được gọi là kiểu suy luận . 
 *  Về cơ bản, TypeScript đoán kiểu của biến. 
 */
'let result;'

/**
 *  - Nếu bạn khai báo một biến với objectkiểu, 
 *      bạn cũng có thể gán cho nó bất kỳ giá trị nào. 
 *  - Bạn không thể gọi một phương thức trên đó ngay cả khi phương thức đó thực sự tồn tại.
 */
 let result: any;
 result = 10.123;
 console.log(result.toFixed());
 result.willExist();

 // Trình biên dịch TypeScript không đưa ra bất kỳ cảnh báo nào 
 // ngay cả khi willExist() phương thức không tồn tại tại thời điểm biên dịch 
 // vì willExist()phương thức này có thể khả dụng trong thời gian chạy.