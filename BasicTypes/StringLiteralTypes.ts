// Các kiểu ký tự chuỗi cho phép bạn xác định một kiểu chỉ chấp nhận một ký tự chuỗi được chỉ định.

// Phần sau định nghĩa một kiểu ký tự chuỗi chấp nhận một chuỗi ký tự 'click':
let click: 'click';

// Các kiểu chuỗi ký tự có thể kết hợp độc đáo với các kiểu liên hợp để xác định một tập hợp hữu hạn các giá trị chuỗi ký tự cho một biến:
let mouseEvent: 'click' | 'dblclick' | 'mouseup' | 'mousedown';
mouseEvent = 'click'; // valid
mouseEvent = 'dblclick'; // valid
mouseEvent = 'mouseup'; // valid
mouseEvent = 'mousedown'; // valid
mouseEvent = 'mouseover'; // compiler error

// Nếu bạn sử dụng các kiểu ký tự chuỗi ở nhiều nơi, chúng sẽ rất dài dòng.
// Để tránh điều này, bạn có thể sử dụng loại bí danh:
type MouseEvent: 'click' | 'dblclick' | 'mouseup' | 'mousedown';
let mouseEvent: MouseEvent;
mouseEvent = 'click'; // valid
mouseEvent = 'dblclick'; // valid
mouseEvent = 'mouseup'; // valid
mouseEvent = 'mousedown'; // valid
mouseEvent = 'mouseover'; // compiler error

let anotherEvent: MouseEvent;