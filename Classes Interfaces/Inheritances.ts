// Một lớp có thể sử dụng lại các thuộc tính và phương thức của một lớp khác.
// Đây được gọi là kế thừa trong TypeScript.


// Giả sử bạn có lớp Person sau:
class Person {
    constructor(private firstName: string, private lastName: string) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
    getFullName(): string {
        return `${this.firstName} ${this.lastName}`;
    }
    describe(): string {
        return `This is ${this.firstName} ${this.lastName}.`;
    }
}

// Để kế thừa một lớp, bạn sử dụng từ khóa extends.

/**
 *  CONSTRUCTOR
 *  Vì Personlớp có một phương thức khởi tạo các thuộc tính firstName và lastName
 *  bạn cần khởi tạo các thuộc tính này trong phương thức khởi tạo của lớp Employee
 *  bằng cách gọi phương thức khởi tạo của lớp cha của nó.
 */
 class Employee extends Person {
    constructor(
        firstName: string,
        lastName: string,
        private jobTitle: string) {
        
    // Để gọi hàm tạo của lớp cha trong hàm tạo của lớp con, bạn sử dụng super().
        super(firstName, lastName);
    }
}
// Vì Employeelớp kế thừa các thuộc tính và phương thức của lớp Person,
// bạn có thể gọi các phương thức getFullName() và describe() trên đối tượng employee
let employee = new Employee('John', 'Doe', 'Web Developer');

console.log(employee.getFullName());
console.log(employee.describe());
