/**
 *  TypeScript cung cấp công cụ sửa đổi readonly cho phép bạn đánh dấu các thuộc tính của một lớp là bất biến.
 *   Việc gán cho thuộc tính readonly chỉ có thể xảy ra ở một trong hai nơi:
 *      - Trong property declaration
 *      - Trong hàm tạo của cùng một lớp.
 */
 class Person_Readonly1 {
    readonly birthDate: Date;

    constructor(birthDate: Date) {
        this.birthDate = birthDate;
    }
}
// Bạn có thể hợp nhất việc khai báo và khởi tạo thuộc tính chỉ đọc trong phương thức khởi tạo như sau:
class Person_Readonly2 {
    constructor(readonly birthDate: Date) {
        this.birthDate = birthDate;
    }
}