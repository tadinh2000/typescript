/**
 *  - Một lớp trừu tượng thường được sử dụng để xác định các hành vi chung
 *       cho các lớp dẫn xuất để mở rộng.
 *  - Một lớp trừu tượng không thể được khởi tạo trực tiếp.
 */
abstract class Employee {
    constructor(private firstName: string, private lastName: string) {
    }
    abstract getSalary(): number
    get fullName(): string {
        return `${this.firstName} ${this.lastName}`;
    }
    compensationStatement(): string {
        return `${this.fullName} makes ${this.getSalary()} a month.`;
    }
}
/**
 *  Trong lớp Employee:
 *  - Hàm tạo khai báo các thuộc tính firstNamevà lastName.
 *  - Phương thức getSalary() là một phương pháp trừu tượng.
 *          Lớp dẫn xuất sẽ thực hiện logic dựa trên kiểu nhân viên.
 *  - Các phương pháp getFullName() và compensationStatement() chứa câu lệnh chi tiết.
 *          Lưu ý rằng phương thức compensationStatement() gọi phương thức getSalary().
 */

// Bởi vì Employeelớp là trừu tượng, bạn không thể tạo một đối tượng mới từ nó. 


// Lớp FullTimeEmployee thừa kế từ lớp Employee:
class FullTimeEmployee extends Employee {
    // Lương được đặt trong hàm tạo
    constructor(firstName: string, lastName: string, private salary: number) {
        super(firstName, lastName);
    }
    // getSalary() là một phương thức trừu tượng của lớp Employee
    // nên lớp FullTimeEmployee cần triển khai phương thức này.
    getSalary(): number {
        return this.salary;
    }
}


// Lớp Contractor cũng kế thừa từ lớp Employee:
class Contractor extends Employee {
    // Hàm khởi tạo khởi tạo tỷ lệ và giờ.
    constructor(firstName: string, lastName: string, private rate: number, private hours: number) {
        super(firstName, lastName);
    }
    // Phương pháp getSalary() tính lương bằng cách nhân tỷ lệ với giờ.
    getSalary(): number {
        return this.rate * this.hours;
    }
}

let john = new FullTimeEmployee('John', 'Doe', 12000);
let jane = new Contractor('Jane', 'Doe', 100, 160);

console.log(john.compensationStatement());
console.log(jane.compensationStatement());