/**
 *  TypeScript cung cấp ba công cụ sửa đổi quyền truy cập cho các thuộc tính và phương thức
 *  của lớp là private:, protectedvà public:
 *  - Công cụ private sửa đổi cho phép truy cập trong cùng một lớp.
 *  - Công cụ protected sửa đổi cho phép truy cập trong cùng một lớp và các lớp con.
 *  - Công cụ public sửa đổi cho phép truy cập từ bất kỳ vị trí nào.
 */

// Private
class Person1 {
    private ssn: string;
    private firstName: string;
    private lastName: string;
    // ...
}

// Public
class Person2 {
    // ...
    public firstName: string;
    public lastName: string;
    public getFullName(): string {
        return `${this.firstName} ${this.lastName}`; 
    }
    // ...
}

// Protected 
class Person3 {

    protected ssn: string;
    
    // other code
}


// Để làm cho mã ngắn hơn, TypeScript cho phép bạn vừa khai báo thuộc tính 
// vừa khởi tạo chúng trong hàm tạo như sau:
class Person4 {
    constructor(protected ssn: string, private firstName: string, private lastName: string) {
        this.ssn = ssn;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    getFullName(): string {
        return `${this.firstName} ${this.lastName}`;
    }
}