// Static property
// Để khai báo một static property, bạn sử dụng statictừ khóa.
// Để truy cập static property, bạn sử dụng cú pháp className.propertyName.
class Employee_property {
    static headcount: number = 0;

    constructor(
        private firstName: string,
        private lastName: string,
        private jobTitle: string) {

            Employee_property.headcount++;
    }
}
let john = new Employee_property('John', 'Doe', 'Front-end Developer');
let jane = new Employee_property('Jane', 'Doe', 'Back-end Developer');

console.log(Employee_property.headcount); // 2

// Static methods
// Tương tự như static property, một Static methods cũng được chia sẻ trên các phiên bản của lớp.
// Để khai báo một Static methods, bạn sử dụng từ khóa static trước tên phương thức.
class Employee_methods {
    private static headcount: number = 0;

    constructor(
        private firstName: string,
        private lastName: string,
        private jobTitle: string) {

            Employee_methods.headcount++;
    }

    public static getHeadcount() {
        return Employee_methods.headcount;
    }
}
let john2 = new Employee_methods('John', 'Doe', 'Front-end Developer');
let jane2 = new Employee_methods('Jane', 'Doe', 'Back-end Developer');

console.log(Employee_methods.getHeadcount); // 2
